import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile
from threading import Thread
import os
from selenium import webdriver
import time
import glob
from time import strftime
import getpass

#function to scrape the cisco active advisor's webpage and perform activities like loggin in and exporting the details into XLS file.
def scrapit(uname,passwd):
    driver = webdriver.Chrome()			#getting the chrome driver
    driver.get("https://ciscoactiveadvisor.com/ux/app/#login")			#accessing CAA login page
    username = driver.find_element_by_id("username")			#getting into the username tab, where our username needs to be entered
    username.clear()			#clearing the existing entries
    username.send_keys(uname)			#entering our username
    password = driver.find_element_by_name("password")			#getting into the password tab, where our password needs to be entered
    password.clear()			#clearing the existing entries
    password.send_keys(passwd)			#entering our password
    try:
        driver.find_element_by_id("login").click()			#clicking the 'login' button
        print("Login to CAA page successful")
    except:
        print("Login to CAA page failed. Check your credentials,url link and try again!!")
    driver.get("https://ciscoactiveadvisor.com/ux/app/#devices")			#accessing the 'devices' page, where all our devices information is stored
    time.sleep(5)
    driver.execute_script('op.deviceFrame.exportAll("XLS");return false')			#grabbing the 'Save ALL as XLS' button, which needs to be clicked to export the details into xls file
    time.sleep(5)
    driver.close()			#closing the webdriver

#function to parse the downloaded file from Cisco Active Advisor and converting into a summary sheet
def parsingsheet(sheet,sheetnumber):
	#creating the list of required data from downloaded file
	requiredfields = ['End Of Sale','End Of Support','IP Address','Serial Number','Source','Type','Warnings','Contract Start','Contract End','Status','Announce Date','Service Renewal','Migration PID','Migration URL','Bulletin URL','Warranty Start','Warranty End']
	#parsing one particular sheet in the downloaded file
	df = xls.parse(sheet)
	#converting data into dictionary format, so that it will be easy to parse and iterate
	dff = df.to_dict()
	#'rowcount' is a variable which tells us how many entries or rows are present in this sheet
	rowcount = 0
	for x in dff['Network: Uncategorized'].keys():
		rowcount = rowcount + 1
	#'i' is counter which is used to iterate through the all entries
	i = 1
	#for first sheet we need to do specific jobs like storing 'Devicename_PID_serialnumber', from other sheets we just take data and map it to these
	if sheetnumber == 1:
		while rowcount > 1:
			#initializing a new dictionary per row
			dict = 'dict' + str(i)
			dict = {}
			#device is the key used for 'details' dictionary in upcoming part of code, we are keeping devicenames with serialnumbers and PID as keys
			device = dff['Network: Uncategorized'][i]
			#iterating through the sheet and storing the values in the dictionary
			for key in dff.keys():
				#checking whether the value we are looking at is a required field for our summary
				if dff[key][0] in requiredfields:
					dict[dff[key][0]] = str(dff[key][i])
					#making sure that there won't be any repeteation in the column headers of our summary sheet
					if dff[key][0] not in values:
						values.append(dff[key][0])
				else:
					continue
			i = i + 1
			#adding the dictionaries
			details[device] = dict
			rowcount = rowcount - 1
	#if this is not the first sheet, no need to worry about first column now, since its already been created, just map the values
	else:
		while rowcount > 1:
			for key in dff.keys():
				#checking whether the column we are dealing with is a required field
				if dff[key][0] in requiredfields:
					#checking whether the column header is already present in the 'values' list
					if dff[key][0] not in values:
						#since we have two columns among all sheets with 'status' has header, but both these columns are unique
						#one status tells us 'contract status' and other is 'warranty status'
						if sheet == 'Device List (Contracts)' and dff[key][0] == 'Status':
							details[dff['Network: Uncategorized'][i]]['Contract Status'] = str(dff[key][i])
							if 'Contract Status' not in values:
								values.append('Contract Status')
						elif sheet == 'Device List (Warranty)' and dff[key][0] == 'Status':
							details[dff['Network: Uncategorized'][i]]['Warranty Status'] = str(dff[key][i])
							if 'Warranty Status' not in values:
								values.append('Warranty Status')
						else:
							details[dff['Network: Uncategorized'][i]][dff[key][0]] = str(dff[key][i])
							values.append(dff[key][0])
					#if column header is already present in the 'values' list
					else:
						details[dff['Network: Uncategorized'][i]][dff[key][0]] = str(dff[key][i])
				else:
					continue
			i = i + 1
			rowcount = rowcount-1

#asking user for the username to CAA webpage
uname = input("Please enter your CAA username here: \n")
#asking for the password, 'getpass' module here takes care that password won't be visible on the terminal while running the script.
passwd = getpass.getpass('Enter your password here:')
#calling the 'scrapit' function where we will go to CAA page and export the devices info
scrapit(uname,passwd)
#asking the user for his/her path on laptop for downloads
downloadspath = input("Please enter the path to your downloads here: \n")
#adding * at the end of downloads path
filespath = os.path.join(downloadspath,'*')
#'glob' module here will help us to select all files in downloads and choose the latest one
list_of_files = glob.glob(filespath)
#latest_file is the latest downloaded file, i.e., the file we exported from CAA
latest_file = max(list_of_files, key=os.path.getctime)
#Excelfile in pandas will help us reading the xls file we exported
xls = ExcelFile(latest_file)
sheetcount = 0
#this is to find out how many sheets are there in the exported xls file
for sheet in xls.sheet_names:
	sheetcount = sheetcount+1
#details is the dictionary, which contains all info at the end of parsing, making it a global variable
global details
details = {}
sheetnumber = 0
#values is the list of all column headers, making it a global variable
global values
values = []
#initializing the threads, which is the list of all separate threads
threads = []
#iterating through the various sheets in the file, and initiating one thread per sheet, enabling parallel processing
for sheet in xls.sheet_names:
	sheetnumber = sheetnumber+1
	#since only few sheets contain the data which is required for us, so checking if the sheetname is equal to any of these
	if sheet == "Device List (All Devices)" or sheet == "Device List (Contracts)" or sheet == "Device List (Warranty)" or sheet == "Device List (Hardware - EOL)":
		if sheetnumber == 1:
			parsing = parsingsheet(sheet,sheetnumber)
		else:
			parsing = Thread(target=parsingsheet,args=[sheet,sheetnumber])
			parsing.start()
			threads.append(parsing)
	else:
		continue
for thread in threads:
	thread.join()
#saving the parsed data into a csv file
#converting the parsed data which is in dictionary format into a dataframe
data = pd.DataFrame(details)
#getting the transpose of this dataframe
finaldata = data.T
#getting current working directory
path = os.getcwd()
#getting current timestamp
timestamp = strftime("%Y-%m-%d_%H_%M_%S")
#creating filename in desired format 'lcm_timestamp.csv'
savefile = "lcm_"+str(timestamp)+".csv"
savepath = os.path.join(path,savefile)
#saving to csv file
finaldata.to_csv(savepath)
print("Details stored in file by name "+str(savefile))


